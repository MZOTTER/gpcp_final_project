# GPCP Analysis

This project is a tutorial for making monthly averages, climatologies, and anomalies for global precipitation, and can be used for larger datasets.

This project makes extensive use of matplotlib, xarray, and numpy. Cartopy and pandas are also utilized to some extent. Files are retrieved from the [NOAA GPCP website](https://www.ncei.noaa.gov/data/global-precipitation-climatology-project-gpcp-daily/access/)

An exmaple of using GPCP in regards to ENSO is featured as an example. Below are processes for how to make netcdf files for monthly averages, climatologies, and anomalies.

## Usage

```python
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from cartopy import crs as ccrs, feature as cfeature
import pandas as pd
from datetime import datetime as dt
import xarray as xr

#For loop for monthly averages
for i in year:
    # step 1: get the list of files of all daily files within each year 
    dir = path + str(i)
    # step 2: open all daily files within a year
    ds = xr.open_mfdataset(dir + '/*.nc', parallel=True)
    # step 3: set your precip variable to ds.precip
    precip = ds.precip
    # step 4: following the steps that you have doing the masking, calculate monthly average
    monthly_avg = precip.resample(time="1MS", skipna=True).mean()
    monthly_avg.to_netcdf(new_path + str(i) + "_monthly_avg.nc")

#10 year climatologies.
ds_10yr = ds.precip.sel(time=slice("1997-01-01", "2006-12-01"))
dsclim_10 = ds_10yr.groupby("time.month").mean(dim="time", skipna=True)
dsclim_10.to_netcdf(new_path + '1997-2006_climatology_mean.nc')

#For loop for monthly averages
year = np.arange(1997,2007)
for i in year:
    ds_1yr = ds.precip.sel(time=slice(str(i) + '-01-01', str(i) + '-12-01'))
    ds_1yranom = ds_1yr.groupby("time.month").mean(dim="time", skipna=True)
    precip_anom = ds_1yranom - precip_clim
    precip_anom.to_netcdf(new_path + str(i) + "_anomaly.nc")

```

## Contributing

This project can be used for any scientific purpose or as a tutorial to present to other classes.

## License

[MIT](https://choosealicense.com/licenses/mit/)
